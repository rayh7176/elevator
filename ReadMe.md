## Table of Contents

 Due to the short period of time required to build the application, I was not able to finish the Lift Algo part.

 The application consists of control of an elevator

 - All access to the application was designed via the API

 - Command folder contains classes for manipulating data access

 - IO for data access from and to the file system

 - Model and control folders contain the manipulation and controls for the lift

1. Before you build it
 - Required JDK 1.8.
 - Required Maven 3.x.x

2. Building
 - mvn clean install -DskipTests=false
3. Running
 - Only from tests the application can be run
4. Troubleshooting

5. Environments
 - DEV




