package com.boa.assignment.control;

import com.boa.assignment.api.ElevatorControlSystem;
import com.boa.assignment.api.ElevatorRequest;
import com.boa.assignment.api.InputCommands;
import com.boa.assignment.api.OutputCommands;
import com.boa.assignment.command.ElevatorRequestImpl;
import com.boa.assignment.command.OutputCommandsImpl;
import com.boa.assignment.model.Building;
import com.boa.assignment.model.ElevatorImpl;
import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;

import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.LinkedList;
import java.util.Queue;

import static org.mockito.Mockito.when;

/**
 * Created by rayan on 23/06/16.
 */
@RunWith(MockitoJUnitRunner.class)
public class ElevatorControlSystemImplModeATest {

    @Mock
    InputCommands<ElevatorRequest, Building.Floor> input1;
    @Mock
    InputCommands<ElevatorRequest, Building.Floor> input2;
    @Mock
    InputCommands<ElevatorRequest, Building.Floor> input3;
    @Mock
    InputCommands<ElevatorRequest, Building.Floor> input4;
    @Mock
    InputCommands<ElevatorRequest, Building.Floor> input5;
    @Mock
    InputCommands<ElevatorRequest, Building.Floor> input6;

    InputCommands<ElevatorRequest, Building.Floor> [] inputCommands ;
    OutputCommands<Integer> outputCommand;

    @Captor
    ArgumentCaptor argCaptor;

    @Before
    public void setUp(){
        outputCommand = new OutputCommandsImpl();
    }

    /**
     * DATA 10:8-1
     * RESULTS 10 8 1 (9)
     */
    @Test
    public void testVisitFloorOneInput(){
        inputCommands = new InputCommands[] {input1};
        int expectedVisitedFloors = 9;
        ElevatorRequestImpl request = new ElevatorRequestImpl(8,1);
        when(input1.initFloor()).thenReturn(Building.Floor.TENTH);

        when(input1.isEmpty()).thenReturn(false).thenReturn(true);

        when(input1.peek()).thenReturn(request);
        when(input1.poll()).thenReturn(request);

        // Create an elevator with mode A
        ElevatorControlSystem controlSystem = new ElevatorControlSystemImpl(ElevatorControlSystem.ElevatorMode.MODE_A,outputCommand);
        controlSystem.visitFloors(inputCommands);

        Assert.assertEquals(expectedVisitedFloors, controlSystem.getNumberOfFloorsVisited());

    }
//
//    /**
//     * DATA: 9:1-5,1-6,1-5
//     */
//    @Test
//    public void testVisitFloorOneInputThreeCommandsTypeOne(){
//        inputCommands = new InputCommands[] {input1};
//        int expectedVisitedFloors = 30;
//
//        when(input1.getInitLocation()).thenReturn(9);
//
//        when(input1.isEmpty()).thenReturn(false).thenReturn(false).thenReturn(false).thenReturn(true);
//        when(input1.retrieveCommand()).thenReturn(new int[]{1, 5}).thenReturn(new int [] {1,6}).thenReturn(new int[]{1, 5});
//
//        // Create an elevator with mode A
//        ElevatorControlSystem controlSystem = new ElevatorControlSystemImpl(ElevatorControlSystem.ElevatorMode.MODE_A,outputCommand);
//        controlSystem.visitFloors(inputCommands);
//
//        Assert.assertEquals(expectedVisitedFloors, controlSystem.getNumberOfFloorsVisited());
//
//    }
//
//    /**
//     * DATA 2:4-1,4-2,6-8
//     */
//    @Test
//    public void testVisitFloorOneInputThreeCommandsTypeTwo(){
//        inputCommands = new InputCommands[] {input1};
//        int expectedVisitedFloors = 16;
//
//        when(input1.getInitLocation()).thenReturn(2);
//
//        when(input1.isEmpty()).thenReturn(false).thenReturn(false).thenReturn(false).thenReturn(true);
//        when(input1.retrieveCommand()).thenReturn(new int[]{4,1}).thenReturn(new int [] {4,2}).thenReturn(new int[]{6,8});
//
//        // Create an elevator with mode A
//        ElevatorControlSystem controlSystem = new ElevatorControlSystemImpl(ElevatorControlSystem.ElevatorMode.MODE_A,outputCommand);
//        controlSystem.visitFloors(inputCommands);
//
//        Assert.assertEquals(expectedVisitedFloors, controlSystem.getNumberOfFloorsVisited());
//
//    }
//
//
//    /**
//     * DATA 3:7-9,3-7,5-8,7-11,11-1
//     */
//    @Test
//    public void testVisitFloorOneInputThreeCommandsTypeThree(){
//        inputCommands = new InputCommands[] {input1};
//        int expectedVisitedFloors = 36;
//
//        when(input1.getInitLocation()).thenReturn(3);
//
//        when(input1.isEmpty()).thenReturn(false).thenReturn(false).thenReturn(false).thenReturn(false).thenReturn(false).thenReturn(true);
//        when(input1.retrieveCommand()).thenReturn(new int[]{7,9}).thenReturn(new int [] {3,7}).thenReturn(new int[]{5,8}).thenReturn(new int[]{7, 11}).thenReturn(new int[]{11,1});
//
//        // Create an elevator with mode A
//        ElevatorControlSystem controlSystem = new ElevatorControlSystemImpl(ElevatorControlSystem.ElevatorMode.MODE_A,outputCommand);
//        controlSystem.visitFloors(inputCommands);
//
//        Assert.assertEquals(expectedVisitedFloors, controlSystem.getNumberOfFloorsVisited());
//
//    }
//
//    /**
//     * DATA 7:11-6,10-5,6-8,7-4,12-7,8-9
//     */
//    @Test
//    public void testVisitFloorOneInputThreeCommandsTypeFour(){
//        inputCommands = new InputCommands[] {input1};
//        int expectedVisitedFloors = 40;
//
//        when(input1.getInitLocation()).thenReturn(7);
//
//        when(input1.isEmpty()).thenReturn(false).thenReturn(false).thenReturn(false).thenReturn(false).thenReturn(false).thenReturn(false).thenReturn(true);
//        when(input1.retrieveCommand()).thenReturn(new int[]{11,6}).thenReturn(new int [] {10,5}).thenReturn(new int[]{6,8}).thenReturn(new int[]{7, 4}).thenReturn(new int[]{12,7}).thenReturn(new int[]{8,9});
//
//        // Create an elevator with mode A
//        ElevatorControlSystem controlSystem = new ElevatorControlSystemImpl(ElevatorControlSystem.ElevatorMode.MODE_A,outputCommand);
//        controlSystem.visitFloors(inputCommands);
//
//        Assert.assertEquals(expectedVisitedFloors, controlSystem.getNumberOfFloorsVisited());
//
//    }
//
//    /**
//     * DATA 6:1-8,6-8
//     */
//    @Test
//    public void testVisitFloorOneInputThreeCommandsTypeFive(){
//        inputCommands = new InputCommands[] {input1};
//        int expectedVisitedFloors = 16;
//
//        when(input1.getInitLocation()).thenReturn(6);
//
//        when(input1.isEmpty()).thenReturn(false).thenReturn(false).thenReturn(true);
//        when(input1.retrieveCommand()).thenReturn(new int[]{1,8}).thenReturn(new int [] {6,8});
//
//        // Create an elevator with mode A
//        ElevatorControlSystem controlSystem = new ElevatorControlSystemImpl(ElevatorControlSystem.ElevatorMode.MODE_A,outputCommand);
//        controlSystem.visitFloors(inputCommands);
//
//        Assert.assertEquals(expectedVisitedFloors, controlSystem.getNumberOfFloorsVisited());
//
//    }
//
//    //////////////////////////////////////////////// TWO Inputs
//
//    /**
//     * Input1: 10:8-1
//     * Input2: 9:1-5,1-6,1-5
//     * Output (9) + (30) = 39
//     */
//    @Test
//    public void testVisitFloorTwoInput(){
//
//        inputCommands = new InputCommands[] {input1, input2};
//
//        int expectedVisistedFloors = 30;
//        when(input1.getInitLocation()).thenReturn(10);
//        when(input1.isEmpty()).thenReturn(false).thenReturn(true);
//        when(input1.retrieveCommand()).thenReturn(new int [] {8,1});
//
//        /////////////////////////////////////
//
//        when(input2.getInitLocation()).thenReturn(9);
//        when(input2.isEmpty()).thenReturn(false).thenReturn(false).thenReturn(false).thenReturn(true);
//        when(input2.retrieveCommand()).thenReturn(new int[]{1, 5}).thenReturn(new int [] {1,6}).thenReturn(new int[]{1, 5});
//
//
//        // Create an elevator with mode A
//        ElevatorControlSystem controlSystem = new ElevatorControlSystemImpl(ElevatorControlSystem.ElevatorMode.MODE_A,outputCommand);
//        controlSystem.visitFloors(inputCommands);
//
//        Integer[][] results  = outputCommand.getAllResults();
//
//        Integer [] result0 = results[0];
//        Integer [] result1 = results[1];
//
//        // EXPECTED 10 8 1 (9)
//        Assert.assertEquals(10, result0[0].intValue());
//        Assert.assertEquals(8, result0[1].intValue());
//        Assert.assertEquals(1, result0[2].intValue());
//        Assert.assertEquals(9, result0[3].intValue());
//
//        // EXPECTED 9 1 5 1 6 1 5 (30)
//        Assert.assertEquals(9, result1[0].intValue());
//        Assert.assertEquals(1, result1[1].intValue());
//        Assert.assertEquals(5, result1[2].intValue());
//        Assert.assertEquals(1, result1[3].intValue());
//        Assert.assertEquals(6, result1[4].intValue());
//        Assert.assertEquals(1, result1[5].intValue());
//        Assert.assertEquals(5, result1[6].intValue());
//        Assert.assertEquals(30, result1[7].intValue());
//        Assert.assertEquals(expectedVisistedFloors, controlSystem.getNumberOfFloorsVisited());
//
//    }
//
//    //////////////////////////////////////////////// FIVE Inputs
//    /**
//     * Input1: 10:8-1
//     * Input2: 9:1-5,1-6,1-5
//     * Input3: 2:4-1,4-2,6-8
//     * Input4: 3:7-9,3-7,5-8,7-11,11-1
//     * Input5: 7:11-6,10-5,6-8,7-4,12-7,8-9
//     * Input6: 6:1-8,6-8
//     * Output (30) = 39
//     */
//    @Test
//    public void testVisitFloorFiveInput(){
//
//        inputCommands = new InputCommands[] {input1, input2, input3, input4, input5, input6};
//
//        int expectedVisistedFloors = 30;
//        when(input1.getInitLocation()).thenReturn(10);
//        when(input1.isEmpty()).thenReturn(false).thenReturn(true);
//        when(input1.retrieveCommand()).thenReturn(new int [] {8,1});
//
//        /////////////////////////////////////
//
//        when(input2.getInitLocation()).thenReturn(9);
//        when(input2.isEmpty()).thenReturn(false).thenReturn(false).thenReturn(false).thenReturn(true);
//        when(input2.retrieveCommand()).thenReturn(new int[]{1, 5}).thenReturn(new int [] {1,6}).thenReturn(new int[]{1, 5});
//
//        /////////////////////////////////////
//
//        when(input3.getInitLocation()).thenReturn(2);
//        when(input3.isEmpty()).thenReturn(false).thenReturn(false).thenReturn(false).thenReturn(true);
//        when(input3.retrieveCommand()).thenReturn(new int[]{4,1}).thenReturn(new int [] {4,2}).thenReturn(new int[]{6,8});
//
//        /////////////////////////////////////
//
//        when(input4.getInitLocation()).thenReturn(3);
//        when(input4.isEmpty()).thenReturn(false).thenReturn(false).thenReturn(false).thenReturn(false).thenReturn(false).thenReturn(true);
//        when(input4.retrieveCommand()).thenReturn(new int[]{7,9}).thenReturn(new int[]{3, 7}).thenReturn(new int[]{5,8}).thenReturn(new int[]{7, 11}).thenReturn(new int[]{11,1});
//
//        /////////////////////////////////////
//        when(input5.getInitLocation()).thenReturn(7);
//        when(input5.isEmpty()).thenReturn(false).thenReturn(false).thenReturn(false).thenReturn(false).thenReturn(false).thenReturn(false).thenReturn(true);
//        when(input5.retrieveCommand()).thenReturn(new int[]{11,6}).thenReturn(new int [] {10,5}).thenReturn(new int[]{6,8}).thenReturn(new int[]{7, 4}).thenReturn(new int[]{12,7}).thenReturn(new int[]{8,9});
//
//        /////////////////////////////////////
//        when(input6.getInitLocation()).thenReturn(6);
//        when(input6.isEmpty()).thenReturn(false).thenReturn(false).thenReturn(true);
//        when(input6.retrieveCommand()).thenReturn(new int[]{1,8}).thenReturn(new int [] {6,8});
//
//        // Create an elevator with mode A
//        ElevatorControlSystem controlSystem = new ElevatorControlSystemImpl(ElevatorControlSystem.ElevatorMode.MODE_A,outputCommand);
//        controlSystem.visitFloors(inputCommands);
//
//        Integer[][] results  = outputCommand.getAllResults();
//
//        Integer [] result0 = results[0];
//        Integer [] result1 = results[1];
//        Integer [] result2 = results[2];
//        Integer [] result3 = results[3];
//        Integer [] result4 = results[4];
//        Integer [] result5 = results[5];
//
//
//        // EXPECTED 10 8 1 (9)
//        Assert.assertEquals(10, result0[0].intValue());
//        Assert.assertEquals(8, result0[1].intValue());
//        Assert.assertEquals(1, result0[2].intValue());
//        Assert.assertEquals(9, result0[3].intValue());
//
//        // EXPECTED 9 1 5 1 6 1 5 (30)
//        Assert.assertEquals(9, result1[0].intValue());
//        Assert.assertEquals(1, result1[1].intValue());
//        Assert.assertEquals(5, result1[2].intValue());
//        Assert.assertEquals(1, result1[3].intValue());
//        Assert.assertEquals(6, result1[4].intValue());
//        Assert.assertEquals(1, result1[5].intValue());
//        Assert.assertEquals(5, result1[6].intValue());
//        Assert.assertEquals(30, result1[7].intValue());
//
//
//        // EXPECTED 2 4 1 4 2 6 8 (16)
//        Assert.assertEquals(2, result2[0].intValue());
//        Assert.assertEquals(4, result2[1].intValue());
//        Assert.assertEquals(1, result2[2].intValue());
//        Assert.assertEquals(4, result2[3].intValue());
//        Assert.assertEquals(2, result2[4].intValue());
//        Assert.assertEquals(6, result2[5].intValue());
//        Assert.assertEquals(8, result2[6].intValue());
//        Assert.assertEquals(16, result2[7].intValue());
//
//        // EXPECTED 3 7 9 3 7 5 8 7 11 11 1 (36)
//        Assert.assertEquals(3, result3[0].intValue());
//        Assert.assertEquals(7, result3[1].intValue());
//        Assert.assertEquals(9, result3[2].intValue());
//        Assert.assertEquals(3, result3[3].intValue());
//        Assert.assertEquals(7, result3[4].intValue());
//        Assert.assertEquals(5, result3[5].intValue());
//        Assert.assertEquals(8, result3[6].intValue());
//        Assert.assertEquals(7, result3[7].intValue());
//        Assert.assertEquals(11, result3[8].intValue());
//        Assert.assertEquals(11, result3[9].intValue());
//        Assert.assertEquals(1, result3[10].intValue());
//        Assert.assertEquals(36, result3[11].intValue());
//
//        //EXPECTED 7 11 6 10 5 6 8 7 4 12 7 8 9 (40)
//        Assert.assertEquals(7, result4[0].intValue());
//        Assert.assertEquals(11, result4[1].intValue());
//        Assert.assertEquals(6, result4[2].intValue());
//        Assert.assertEquals(10, result4[3].intValue());
//        Assert.assertEquals(5, result4[4].intValue());
//        Assert.assertEquals(6, result4[5].intValue());
//        Assert.assertEquals(8, result4[6].intValue());
//        Assert.assertEquals(7, result4[7].intValue());
//        Assert.assertEquals(4, result4[8].intValue());
//        Assert.assertEquals(12, result4[9].intValue());
//        Assert.assertEquals(7, result4[10].intValue());
//        Assert.assertEquals(8, result4[11].intValue());
//        Assert.assertEquals(9, result4[12].intValue());
//        Assert.assertEquals(40, result4[13].intValue());
//
//        //EXPECTED 6 1 8 6 8 (16)
//        Assert.assertEquals(6, result5[0].intValue());
//        Assert.assertEquals(1, result5[1].intValue());
//        Assert.assertEquals(8, result5[2].intValue());
//        Assert.assertEquals(6, result5[3].intValue());
//        Assert.assertEquals(8, result5[4].intValue());
//        Assert.assertEquals(16, result5[5].intValue());
//    }

}
