package com.boa.assignment.control;

import com.boa.assignment.api.*;
import com.boa.assignment.command.OutputCommandsImpl;
import com.boa.assignment.model.Building;
import com.boa.assignment.command.ElevatorRequestImpl;
import jdk.nashorn.internal.ir.annotations.Ignore;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.when;

/**
 * Created by rayan on 23/06/16.
 */
@RunWith(MockitoJUnitRunner.class)
public class ElevatorControlSystemImplModeBTest {

    @Mock
    InputCommands<ElevatorRequest, Building.Floor> input1;
    @Mock
    InputCommands<ElevatorRequest, Building.Floor> input2;
    @Mock
    InputCommands<ElevatorRequest, Building.Floor> input3;
    @Mock
    InputCommands<ElevatorRequest, Building.Floor> input4;
    @Mock
    InputCommands<ElevatorRequest, Building.Floor> input5;
    @Mock
    InputCommands<ElevatorRequest, Building.Floor> input6;

    InputCommands<ElevatorRequest, Building.Floor> [] inputCommands ;
    OutputCommands<Integer> outputCommand;

    @Captor
    ArgumentCaptor argCaptor;

    @Before
    public void setUp(){
        outputCommand = new OutputCommandsImpl();
    }

    @Test
    public void testVisitFloorOneInput(){
        inputCommands = new InputCommands[] {input1};
        int expectedVisistedFloors = 9;
        ElevatorRequestImpl request = new ElevatorRequestImpl(8,1);
        when(input1.initFloor()).thenReturn(Building.Floor.TENTH);
        when(input1.isEmpty()).thenReturn(true);
        when(input1.peek()).thenReturn(request);
        when(input1.poll()).thenReturn(request);
        when(input1.poll()).thenReturn(request);
        // Create an elevator with mode A
        ElevatorControlSystem controlSystem = new ElevatorControlSystemImpl(ElevatorControlSystem.ElevatorMode.MODE_B,outputCommand);
        controlSystem.visitFloors(inputCommands);

        Assert.assertEquals(expectedVisistedFloors, controlSystem.getNumberOfFloorsVisited());

        Integer[][] results  = outputCommand.getAllResults();

        Integer [] result = results[0];
        // EXPECTED 10 8 1 (9)
        Assert.assertEquals(10, result[0].intValue());
        Assert.assertEquals(8, result[1].intValue());
        Assert.assertEquals(1, result[2].intValue());
        Assert.assertEquals(9, result[3].intValue());
    }

    /**
     * DATA: 9:1-5,1-6,1-5
     */
    @Test
    public void testVisitFloorOneInputThreeCommandsTypeOne(){
        inputCommands = new InputCommands[] {input1};
        int expectedVisitedFloors = 13;

        when(input1.initFloor()).thenReturn(Building.Floor.NINTH);

        ElevatorRequestImpl requestOneToFive = new ElevatorRequestImpl(1,5);
        ElevatorRequestImpl requestOneToSix = new ElevatorRequestImpl(1,6);
        ElevatorRequestImpl requestOneToFive1 = new ElevatorRequestImpl(1,5);

        when(input1.isEmpty()).thenReturn(false).thenReturn(false).thenReturn(true);
        when(input1.peek()).thenReturn(requestOneToSix).thenReturn(requestOneToFive1);
        when(input1.poll()).thenReturn(requestOneToFive).thenReturn(requestOneToSix).thenReturn(requestOneToFive1);

        // Create an elevator with mode B
        ElevatorControlSystem controlSystem = new ElevatorControlSystemImpl(ElevatorControlSystem.ElevatorMode.MODE_B,outputCommand);
        controlSystem.visitFloors(inputCommands);

        Assert.assertEquals(expectedVisitedFloors, controlSystem.getNumberOfFloorsVisited());

        Integer[][] results  = outputCommand.getAllResults();

        Integer [] result = results[0];
        // EXPECTED 9 1 5 6 (13)
        Assert.assertEquals(9, result[0].intValue());
        Assert.assertEquals(1, result[1].intValue());
        Assert.assertEquals(5, result[2].intValue());
        Assert.assertEquals(6, result[3].intValue());

    }
//
    /**
     * DATA 2:4-1,4-2,6-8
     */
    @Test
    public void testVisitFloorOneInputThreeCommandsTypeTwo(){
        inputCommands = new InputCommands[] {input1};
        int expectedVisitedFloors = 12;

        when(input1.initFloor()).thenReturn(Building.Floor.SECOND);

        ElevatorRequestImpl requestFourToOne = new ElevatorRequestImpl(4,1);
        ElevatorRequestImpl requestFourToTwo = new ElevatorRequestImpl(4,2);
        ElevatorRequestImpl requestSixToEight = new ElevatorRequestImpl(6,8);

        when(input1.isEmpty()).thenReturn(false).thenReturn(false).thenReturn(false).thenReturn(true);
        when(input1.peek()).thenReturn(requestFourToTwo).thenReturn(requestSixToEight);
        when(input1.poll()).thenReturn(requestFourToOne).thenReturn(requestFourToTwo).thenReturn(requestSixToEight);

        // Create an elevator with mode A
        ElevatorControlSystem controlSystem = new ElevatorControlSystemImpl(ElevatorControlSystem.ElevatorMode.MODE_B,outputCommand);
        controlSystem.visitFloors(inputCommands);

        Assert.assertEquals(expectedVisitedFloors, controlSystem.getNumberOfFloorsVisited());

        Integer[][] results  = outputCommand.getAllResults();

        Integer [] result2 = results[0];

        // EXPECTED 2 4 2 1 6 8 (12)
        Assert.assertEquals(2, result2[0].intValue());
        Assert.assertEquals(4, result2[1].intValue());
        Assert.assertEquals(2, result2[2].intValue());
        Assert.assertEquals(1, result2[3].intValue());
        Assert.assertEquals(6, result2[4].intValue());
        Assert.assertEquals(8, result2[5].intValue());
        Assert.assertEquals(12, result2[6].intValue());
    }

    /**
     * DATA 3:7-9,3-7,5-8,7-11,11-1
     */
    @Test
    public void testVisitFloorThreeInputThreeCommandsTypeThree(){
        inputCommands = new InputCommands[] {input1};
        int expectedVisitedFloors = 18;

        when(input1.initFloor()).thenReturn(Building.Floor.THIRD);

        ElevatorRequestImpl requestSevenToNine = new ElevatorRequestImpl(7,9);
        ElevatorRequestImpl requestThreeToSeven = new ElevatorRequestImpl(3,7);
        ElevatorRequestImpl requestFiveToEight = new ElevatorRequestImpl(5,8);
        ElevatorRequestImpl requestSevenToEleventh = new ElevatorRequestImpl(7,11);
        ElevatorRequestImpl requestEleventhToOne = new ElevatorRequestImpl(11,1);

        when(input1.isEmpty()).thenReturn(false).thenReturn(false).thenReturn(false).thenReturn(false).thenReturn(false).thenReturn(false).thenReturn(false).thenReturn(false).thenReturn(true);
        when(input1.peek()).thenReturn(requestThreeToSeven).thenReturn(requestFiveToEight).thenReturn(requestSevenToEleventh).thenReturn(requestEleventhToOne);
        when(input1.poll()).thenReturn(requestSevenToNine).thenReturn(requestThreeToSeven).thenReturn(requestFiveToEight).thenReturn(requestSevenToEleventh).thenReturn(requestEleventhToOne);

        // Create an elevator with mode A
        ElevatorControlSystem controlSystem = new ElevatorControlSystemImpl(ElevatorControlSystem.ElevatorMode.MODE_B,outputCommand);
        controlSystem.visitFloors(inputCommands);

        Assert.assertEquals(expectedVisitedFloors, controlSystem.getNumberOfFloorsVisited());

        Integer[][] results  = outputCommand.getAllResults();

        Integer [] result2 = results[0];

        // EXPECTED 3 5 7 8 9 11 1 (18)
        Assert.assertEquals(3, result2[0].intValue());
        Assert.assertEquals(5, result2[1].intValue());
        Assert.assertEquals(7, result2[2].intValue());
        Assert.assertEquals(8, result2[3].intValue());
        Assert.assertEquals(9, result2[4].intValue());
        Assert.assertEquals(11, result2[5].intValue());
        Assert.assertEquals(1, result2[6].intValue());
        Assert.assertEquals(18, result2[7].intValue());

    }
    // Data: 6:1-8,6-8
    // Expected: 6 1 6 8 (12)
    @Test
    public void testVisitTwoFloorsOneInput(){
        inputCommands = new InputCommands[] {input1};
        int expectedVisitedFloors = 12;

        ElevatorRequestImpl requestOneToEight = new ElevatorRequestImpl(1,8);
        ElevatorRequestImpl requestSixToEight = new ElevatorRequestImpl(6,8);

        when(input1.initFloor()).thenReturn(Building.Floor.SIXTH);

        when(input1.isEmpty()).thenReturn(false).thenReturn(true);
        when(input1.peek()).thenReturn(requestSixToEight);
        when(input1.poll()).thenReturn(requestOneToEight).thenReturn(requestSixToEight);

        // Create an elevator with mode A
        ElevatorControlSystem controlSystem = new ElevatorControlSystemImpl(ElevatorControlSystem.ElevatorMode.MODE_B,outputCommand);
        controlSystem.visitFloors(inputCommands);

        Assert.assertEquals(expectedVisitedFloors, controlSystem.getNumberOfFloorsVisited());

        Integer[][] results  = outputCommand.getAllResults();

        Integer [] result = results[0];
        // EXPECTED 10 8 1 (9)
        Assert.assertEquals(6, result[0].intValue());
        Assert.assertEquals(1, result[1].intValue());
        Assert.assertEquals(6, result[2].intValue());
        Assert.assertEquals(8, result[3].intValue());
        Assert.assertEquals(12, result[4].intValue());
    }

    /**
     * DATA 7:11-6,10-5,6-8,7-4,12-7,8-9
     */
    @Test
    @Ignore
    public void testVisitFloorOneInputThreeCommandsTypeFour(){
        inputCommands = new InputCommands[] {input1};
        int expectedVisitedFloors = 30;

        ElevatorRequestImpl requestEleventhToSixt = new ElevatorRequestImpl(11,6);
        ElevatorRequestImpl requestTenToFive = new ElevatorRequestImpl(10,5);
        ElevatorRequestImpl requestSixToEight = new ElevatorRequestImpl(6,8);
        ElevatorRequestImpl requestSevenToFour = new ElevatorRequestImpl(7,4);
        ElevatorRequestImpl requestTwelveToSeven = new ElevatorRequestImpl(12,7);
        ElevatorRequestImpl requestEightToNine = new ElevatorRequestImpl(8,9);

        when(input1.initFloor()).thenReturn(Building.Floor.SEVENTH);


        when(input1.isEmpty()).thenReturn(false).thenReturn(false).thenReturn(false).thenReturn(false).thenReturn(false).thenReturn(true);
        when(input1.peek()).thenReturn(requestTenToFive).thenReturn(requestSevenToFour)
                .thenReturn(requestTwelveToSeven)
                .thenReturn(requestEightToNine);
        when(input1.poll()).thenReturn(requestEleventhToSixt).thenReturn(requestTenToFive).thenReturn(requestSixToEight)
                .thenReturn(requestSevenToFour)
                .thenReturn(requestTwelveToSeven)
                .thenReturn(requestEightToNine)
                ;


        // Create an elevator with mode A
        ElevatorControlSystem controlSystem = new ElevatorControlSystemImpl(ElevatorControlSystem.ElevatorMode.MODE_B,outputCommand);
        controlSystem.visitFloors(inputCommands);

        Assert.assertEquals(expectedVisitedFloors, controlSystem.getNumberOfFloorsVisited());

        Integer[][] results  = outputCommand.getAllResults();

        Integer [] result4 = results[0];
        //INPUT 7:11-6,10-5,6-8,7-4,12-7,8-9
        //EXPECTED 7 11 10 6 5 6 8 12 7 4 8 9 (30)
        Assert.assertEquals(7, result4[0].intValue());
        Assert.assertEquals(11, result4[1].intValue());
        Assert.assertEquals(10, result4[2].intValue());
        Assert.assertEquals(6, result4[3].intValue());
        Assert.assertEquals(5, result4[4].intValue());
        Assert.assertEquals(6, result4[5].intValue());
        Assert.assertEquals(8, result4[6].intValue());
        Assert.assertEquals(12, result4[7].intValue());
        Assert.assertEquals(7, result4[8].intValue());
        Assert.assertEquals(4, result4[9].intValue());
        Assert.assertEquals(8, result4[10].intValue());
        Assert.assertEquals(9, result4[11].intValue());
        Assert.assertEquals(30, result4[12].intValue());

    }
//
}
