package com.boa.assignment.io;

import com.boa.assignment.api.Elevator;
import com.boa.assignment.api.ElevatorRequest;
import com.boa.assignment.api.InputCommands;
import com.boa.assignment.api.OutputCommands;
import com.boa.assignment.model.Building;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Stream;

import com.boa.assignment.command.InputCommandsImpl;

/**
 * IOProcessor is used to read and write data to the file,
 * Created by rayan on 23/06/16.
 */
public class InputOutputDataProcessor {

    private static final Logger LOGGER = LoggerFactory.getLogger(InputOutputDataProcessor.class);

    private final String fileInputPath;

    private static final String UTF_EIGHT = "UTF-8";
    private final String fileOutputPath;


    public InputOutputDataProcessor(String fileInputPath, String fileOutputPath){
        this.fileInputPath = fileInputPath;
        this.fileOutputPath = fileOutputPath;
    }

    public InputCommands fileReadCommands(){
        InputCommands<ElevatorRequest<Elevator.DirectionOfTravel, Building.Floor>, Building.Floor>  inputCommands = null;
        try {
            Objects.nonNull(fileInputPath);

            File file = new File(fileInputPath);
            if (file.exists()) {
                LineIterator it = null;

                    it = FileUtils.lineIterator(file, UTF_EIGHT);
                try {
                    while (it.hasNext()) {
                        String line = it.nextLine();
                        if (!processInputCommand(line, inputCommands)) continue;
                    }
                } finally {
                    LineIterator.closeQuietly(it);
                }
            } else {
                throw new IOException("File not found, fileInputPath="+fileInputPath);
            }
        } catch (IOException e) {
            throw new IllegalAccessError("Not able to process the read of the commands from the file, please check, error "+e.toString());
        }
        return inputCommands;
    }

    /**
     * Write all the results to the file
     * @param outputCommands
     */
    public void fileWriteCommands(OutputCommands<Integer> outputCommands){
        try {
            Integer [] [] results = outputCommands.getAllResults();
            File file = new File(fileOutputPath);
            if (file.exists()) {
                for (Integer [] infos : results) {
                    // Construct a string
                    StringBuffer result = new StringBuffer();
                    for(Integer in : infos){
                        result.append(in).append("%");
                    }

                    FileUtils.writeStringToFile(file, result.toString());
                }
            } else
                throw new IOException("File not found, fileOutputPath="+fileOutputPath);
        } catch (IOException e) {
            throw new IllegalAccessError("Not able to process the write of the commands to the file, please check, error "+e.toString());
        }
    }

    private boolean processInputCommand(String line, InputCommands<ElevatorRequest<Elevator.DirectionOfTravel, Building.Floor>, Building.Floor>  inputCommands) {

        Objects.requireNonNull(line);

        String [] tokens = line.split(":");

        if (tokens != null && tokens.length > 2 && NumberUtils.isNumber(tokens[0])) {

            inputCommands = new InputCommandsImpl(tokens[0]);

            String [] steps =  tokens[1].split(",");

            Arrays.stream(steps).forEach(inputCommands::addCommand);

        }
        else {
            LOGGER.warn("NO Init location found, or incorrect value");
            // go to the next line
            return true;
        }
        return false;
    }

}
