package com.boa.assignment.control;

import com.boa.assignment.api.*;
import com.boa.assignment.model.Building;
import com.boa.assignment.model.ElevatorImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by rayan on 23/06/16.
 */
public class ElevatorControlSystemImpl implements ElevatorControlSystem<ElevatorControlSystem.ElevatorMode, Elevator.DirectionOfTravel, Building.Floor> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ElevatorControlSystemImpl.class);

    private final ElevatorMode mode;
    private final OutputCommands<Integer> outputCommands;
    private Elevator<Elevator.DirectionOfTravel, Building.Floor> elevator;

    Queue<Integer> pickupLocations;
    private Queue<Integer> dropOffLocations;

    public ElevatorControlSystemImpl(ElevatorControlSystem.ElevatorMode mode, OutputCommands<Integer> outputCommands){
        this.mode = mode;
        this.pickupLocations = new LinkedList<>();
        this.dropOffLocations = new LinkedList<>();
        this.outputCommands = outputCommands;
    }

    @Override
    public void pickUp(int pickUpFloor) {
        this.pickupLocations.add(pickUpFloor);
    }

    @Override
    public void dropOff(int dropOffFloor) {
        this.dropOffLocations.add(dropOffFloor);
    }

    @Override
    public void visitFloors(InputCommands<ElevatorRequest<Elevator.DirectionOfTravel, Building.Floor>, Building.Floor>... inputCommands) {

        //run over the commands one at the time
        for (InputCommands<ElevatorRequest<Elevator.DirectionOfTravel, Building.Floor>, Building.Floor> inputCommand : inputCommands) {
            //Always get the first element
            elevator = new ElevatorImpl(inputCommand.initFloor());
            ElevatorRequest<Elevator.DirectionOfTravel, Building.Floor> originalRequest = inputCommand.poll();
            elevator.establishElevatorDirection(originalRequest.getPickUp(), originalRequest.getDropOff());

            boolean halt = inputCommand.isEmpty();
            boolean registerOriginal = true;
            if (!halt) {
                elevator.registerTraverse(originalRequest.getPickUp());
                registerOriginal = false;
            }

            while (!halt) {

                //peek
                ElevatorRequest<Elevator.DirectionOfTravel, Building.Floor> peekRequest = inputCommand.peek();
                if (peekRequest != null) {
                    if (ElevatorMode.MODE_B == getMode()) {
                        // now count the floors
                        // am I in the same direction as the elevator
                        if (peekRequest.getDirection() == elevator.getDirection()) {
                            ModeBProcess modeBProcess = new ModeBProcess(inputCommand, originalRequest, registerOriginal, peekRequest).invoke();
                            registerOriginal = modeBProcess.isRegisterOriginal();
                            originalRequest = modeBProcess.getOriginalRequest();

                        } else {
                            if (!registerOriginal) {
                                elevator.visitTheFloor(originalRequest.getDropOff());
                                elevator.registerTraverse(originalRequest.getDropOff());
                                registerOriginal = true;
                            }
                            elevator.changeDirectionOfTravel();
                            originalRequest = inputCommand.poll();
                            elevator.establishElevatorDirection(originalRequest.getPickUp(), originalRequest.getDropOff());
                        }
                        halt = inputCommand.isEmpty();
                    }
                    else if (ElevatorMode.MODE_A == getMode()) {
                        if (peekRequest.getDirection() == elevator.getDirection()) {
                            switch (elevator.getDirection()) {
                                case UP: {
                                }
                                break;
                                case DOWN: {
                                    if (peekRequest.getPickUp().compareTo(originalRequest.getPickUp()) >= 0) {
                                        if (peekRequest.getDropOff().compareTo(originalRequest.getDropOff()) < 0) {
                                            elevator.registerTraverse(originalRequest.getDropOff());
                                            ElevatorRequest<Elevator.DirectionOfTravel, Building.Floor> pulledRequest = inputCommand.poll();
                                            elevator.visitTheFloor(pulledRequest.getDropOff());
                                            elevator.registerTraverse(pulledRequest.getDropOff());
                                        } else {
                                            ElevatorRequest<Elevator.DirectionOfTravel, Building.Floor> pulledRequest = inputCommand.poll();
                                            elevator.registerTraverse(pulledRequest.getDropOff());
                                            elevator.visitTheFloor(pulledRequest.getDropOff());
                                            // elevator.registerTraverse(originalRequest.getDropOff());
                                        }
                                    } else {
                                        elevator.registerTraverse(originalRequest.getPickUp());
                                        ElevatorRequest<Elevator.DirectionOfTravel, Building.Floor> pulledRequest = inputCommand.poll();
                                        elevator.visitTheFloor(pulledRequest.getDropOff());
                                        elevator.registerTraverse(pulledRequest.getPickUp());
                                        elevator.registerTraverse(originalRequest.getDropOff());
                                        elevator.registerTraverse(pulledRequest.getDropOff());
                                        originalRequest = inputCommand.poll();
                                        elevator.establishElevatorDirection(originalRequest.getPickUp(), originalRequest.getDropOff());
                                        elevator.registerTraverse(originalRequest.getPickUp());
                                    }
                                    break;
                                }

                            }
                        }
                    }

                }
                halt = inputCommand.isEmpty();
            }
            if (halt & registerOriginal) {
                elevator.registerTraverse(originalRequest.getPickUp());
                elevator.visitTheFloor(originalRequest.getDropOff());
                elevator.registerTraverse(originalRequest.getDropOff());
            }
            elevator.registerTotalTraverse();
            Object [] data = elevator.getFloorsVisited().toArray();
                if (data != null)
                    // accumulate the results per command
                    outputCommands.accumulateResults(Arrays.copyOf(data, data.length, Integer[].class));
        }
    }


    @Override
    public ElevatorMode getMode() {
        return mode;
    }

    @Override
    public int getNumberOfFloorsVisited() {
        return elevator.getNumberOfFloorVisited();
    }

    private class ModeBProcess {
        private InputCommands<ElevatorRequest<Elevator.DirectionOfTravel, Building.Floor>, Building.Floor> inputCommand;
        private ElevatorRequest<Elevator.DirectionOfTravel, Building.Floor> originalRequest;
        private boolean registerOriginal;
        private ElevatorRequest<Elevator.DirectionOfTravel, Building.Floor> peekRequest;

        public ModeBProcess(InputCommands<ElevatorRequest<Elevator.DirectionOfTravel, Building.Floor>, Building.Floor> inputCommand, ElevatorRequest<Elevator.DirectionOfTravel, Building.Floor> originalRequest, boolean registerOriginal, ElevatorRequest<Elevator.DirectionOfTravel, Building.Floor> peekRequest) {
            this.inputCommand = inputCommand;
            this.originalRequest = originalRequest;
            this.registerOriginal = registerOriginal;
            this.peekRequest = peekRequest;
        }

        public ElevatorRequest<Elevator.DirectionOfTravel, Building.Floor> getOriginalRequest() {
            return originalRequest;
        }

        public boolean isRegisterOriginal() {
            return registerOriginal;
        }

        public ModeBProcess invoke() {
            switch (elevator.getDirection()) {
                case UP: {
                    if (peekRequest.getPickUp().compareTo(originalRequest.getPickUp()) >= 0) {

                        if (peekRequest.getDropOff().compareTo(originalRequest.getDropOff()) > 0) {
                            elevator.registerTraverse(originalRequest.getDropOff());
                            ElevatorRequest<Elevator.DirectionOfTravel, Building.Floor> pulledRequest = inputCommand.poll();
                            elevator.visitTheFloor(pulledRequest.getDropOff());
                            elevator.registerTraverse(pulledRequest.getDropOff());
                        } else  if ((peekRequest.getDropOff().compareTo(originalRequest.getDropOff()) < 0)){
                            //do nothing
                            ElevatorRequest<Elevator.DirectionOfTravel, Building.Floor> pulledRequest = inputCommand.poll();
                            elevator.registerTraverse(pulledRequest.getPickUp());
                            elevator.visitTheFloor(originalRequest.getDropOff());
                            elevator.registerTraverse(pulledRequest.getDropOff());
                            registerOriginal = false;
                        } else if ((originalRequest.getPickUp().compareTo(peekRequest.getDropOff()) < 0 &&
                                peekRequest.getPickUp().compareTo(originalRequest.getPickUp()) != 0)) {
                            //do nothing
                            ElevatorRequest<Elevator.DirectionOfTravel, Building.Floor> pulledRequest = inputCommand.poll();
                            elevator.registerTraverse(pulledRequest.getPickUp());
                            elevator.visitTheFloor(originalRequest.getDropOff());
                            elevator.registerTraverse(pulledRequest.getDropOff());
                            registerOriginal = false;
                        }
                    } else {
                        if (peekRequest.getDropOff().compareTo(originalRequest.getDropOff()) > 0) {
                            elevator.registerTraverse(originalRequest.getDropOff());
                            ElevatorRequest<Elevator.DirectionOfTravel, Building.Floor> pulledRequest = inputCommand.poll();
                            elevator.visitTheFloor(pulledRequest.getDropOff());
                            elevator.registerTraverse(pulledRequest.getDropOff());
                        } else {
                            //do nothing
                            ElevatorRequest<Elevator.DirectionOfTravel, Building.Floor> pulledRequest = inputCommand.poll();
                            elevator.manipulateTraverse(pulledRequest.getPickUp());
                            elevator.registerTraverse(pulledRequest.getDropOff());
                            registerOriginal = true;
                        }
                    }
                    break;
                }
                case DOWN: {
                    if (peekRequest.getPickUp().compareTo(originalRequest.getPickUp()) >= 0) {
                        if (peekRequest.getDropOff().compareTo(originalRequest.getDropOff()) < 0) {
                            elevator.registerTraverse(originalRequest.getDropOff());
                            ElevatorRequest<Elevator.DirectionOfTravel, Building.Floor> pulledRequest = inputCommand.poll();
                            elevator.visitTheFloor(pulledRequest.getDropOff());
                            elevator.registerTraverse(pulledRequest.getDropOff());
                        } else {
                            ElevatorRequest<Elevator.DirectionOfTravel, Building.Floor> pulledRequest = inputCommand.poll();
                            elevator.registerTraverse(pulledRequest.getDropOff());
                            elevator.visitTheFloor(pulledRequest.getDropOff());
                            // elevator.registerTraverse(originalRequest.getDropOff());
                        }
                    } else {
                        elevator.registerTraverse(originalRequest.getPickUp());
                        ElevatorRequest<Elevator.DirectionOfTravel, Building.Floor> pulledRequest = inputCommand.poll();
                        elevator.visitTheFloor(pulledRequest.getDropOff());
                        elevator.registerTraverse(pulledRequest.getPickUp());
                        elevator.registerTraverse(originalRequest.getDropOff());
                        elevator.registerTraverse(pulledRequest.getDropOff());
                        originalRequest = inputCommand.poll();
                        elevator.establishElevatorDirection(originalRequest.getPickUp(), originalRequest.getDropOff());
                        elevator.registerTraverse(originalRequest.getPickUp());
                    }
                    break;
                }

            }
            return this;
        }
    }
}
