package com.boa.assignment.model;

import com.boa.assignment.api.Elevator;

import java.util.LinkedList;
import java.util.List;

import static com.boa.assignment.model.Building.*;

/**
 * Created by rayan on 23/06/16.
 */
public class ElevatorImpl implements Elevator<Elevator.DirectionOfTravel, Building.Floor> {

    private final LinkedList<Integer> currentFloorList;
    private Floor currentFloor;
    private DirectionOfTravel directionOfTravel;
    private int floorVisited;

    public ElevatorImpl(Floor floorNumber){
        this.currentFloor = floorNumber;
        this.currentFloorList = new LinkedList<>();
        currentFloorList.add(floorNumber.ordinal() + 1);
    }

    public List<Integer> getFloorsVisited() {
        return currentFloorList;
    }

    public Floor getCurrentFloor() {
        return currentFloor;
    }

    @Override
    public void moveUp() {
        directionOfTravel = DirectionOfTravel.UP;
    }

    @Override
    public void moveDown() {
        directionOfTravel = DirectionOfTravel.DOWN;
    }

    @Override
    public int getNumberOfFloorVisited(){
        return floorVisited;
    }

    @Override
    public void changeDirectionOfTravel() {

        if (currentFloor.compareTo(Floor.FIRST) == 0){
            directionOfTravel = DirectionOfTravel.UP;
        } else if (currentFloor.compareTo(Floor.TWELFTH) == 0){
            directionOfTravel = DirectionOfTravel.DOWN;
        }
    }

    public DirectionOfTravel getDirection(){
        return directionOfTravel;
    }

    public void visitTheFloor(Floor pickUp) {

        // visit the floor
        while(this.currentFloor.compareTo(pickUp) != 0 ){
            floorVisited++;
            switch(directionOfTravel){
                case UP:
                {
                    if (currentFloor.compareTo( Floor.TWELFTH) < 0)
                        if (Floor.FIRST.compareTo(currentFloor) == 0)
                            currentFloor = currentFloor.valueOf(currentFloor.ordinal()+2);
                        else
                            currentFloor = currentFloor.valueOf(currentFloor.ordinal()+2);
                    else
                        currentFloor = Floor.TWELFTH;
                    break;
                }
                case DOWN:
                    if ( currentFloor.compareTo(Floor.FIRST) > 0 )
                        if (Floor.TWELFTH.compareTo(currentFloor) == 0)
                            currentFloor = currentFloor.valueOf(currentFloor.ordinal());
                        else
                            currentFloor = Floor.valueOf(currentFloor.ordinal());
                    else
                        currentFloor = Floor.FIRST;
                    break;
            }
        }
        changeDirectionOfTravel();
    }

    public void establishElevatorDirection(Floor pickUp, Floor dropOff) {
        boolean halted = false;
        while (!halted) {
            // check the iniFloor vs First command
            if (getCurrentFloor().compareTo(pickUp) < 0) {
                moveUp();
                visitTheFloor(pickUp);
            } else if (getCurrentFloor().compareTo(pickUp) > 0) {
                moveDown();
                visitTheFloor(pickUp);
            } else {
                halted = true;
                //check the drop off - in all other case - assuming drop off  can never be the same as start point
                if (getCurrentFloor().compareTo(dropOff) < 0) {
                    moveUp();
                } else if (getCurrentFloor().compareTo(dropOff) > 0) {
                    moveDown();
                }
            }
        }
    }

    /**
     * Add all the counter of floor visited
     */
    public void registerTotalTraverse(){
        currentFloorList.add(floorVisited);
    }

    @Override
    public void manipulateTraverse(Floor floor) {
        int pos = currentFloorList.size() - 2;
        int last = 0;
        int current = floor.ordinal() + 1;
        if (pos >=0) {
            //see if it is needed to replace elements
            last = currentFloorList.get(pos);

            if (current > last) {
                last = currentFloorList.removeLast();
                currentFloorList.add(current);
                currentFloorList.add(last);
            }else if (current < last) {
                currentFloorList.add(current);
            }
        }

    }

    @Override
    public void registerTraverse(Floor floor) {
        int current = floor.ordinal() + 1;
        int last = currentFloorList.peekLast();
        if (current != last)
            currentFloorList.add(floor.ordinal() + 1);
    }

    @Override
    public String toString() {
        return "ElevatorImpl{" +
                "currentFloor=" + currentFloor +
                ", directionOfTravel=" + directionOfTravel +
                ", floorVisited=" + floorVisited +
                '}';
    }
}
