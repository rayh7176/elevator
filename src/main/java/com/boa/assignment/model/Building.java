package com.boa.assignment.model;

/**
 * Created by rayan on 23/06/16.
 *
 */
public final class Building {

    /**
     * Floor encapsulates all the logic and data surrounding floors.
     * Floor contains constant names i.e. FIRST for each floor
     */
    public enum Floor {
        FIRST(),
        SECOND(),
        THIRD(),
        FOURTH(),
        FIFTH(),
        SIXTH(),
        SEVENTH(),
        EIGHT(),
        NINTH(),
        TENTH(),
        ELEVENTH(),
        TWELFTH();

        private boolean floorDestinationRequested;

        public boolean isFloorDestinationRequested() {
            return floorDestinationRequested;
        }

        /**
         * Each time the elevator make the floor requested, it change the status to true as true destination
         */
        public void requestFloorDestination() {
            this.floorDestinationRequested = true;
        }

        /**
         * No longer the elevator is requested to stop at the Floor
         */
        public void clearFloorDestination() {
            this.floorDestinationRequested = false;
        }

        public static Floor valueOf(int floorValue){
            switch(floorValue){
                case 1:
                    return Floor.FIRST;
                case 2:
                    return Floor.SECOND;
                case 3:
                    return Floor.THIRD;
                case 4:
                    return Floor.FOURTH;
                case 5:
                    return Floor.FIFTH;
                case 6:
                    return Floor.SIXTH;
                case 7:
                    return Floor.SEVENTH;
                case 8:
                    return Floor.EIGHT;
                case 9:
                    return Floor.NINTH;
                case 10:
                    return Floor.TENTH;
                case 11:
                    return Floor.ELEVENTH;
                case 12:
                    return Floor.TWELFTH;
                default:
                    throw new IllegalArgumentException("Not Supported Floor");
            }
        }

    }

}
