package com.boa.assignment.api;

import java.util.List;

/**
 * Created by rayan on 23/06/16.
 * D: Stand for Elevator Destination
 * F: At what floor the lift is sitting
 */
public interface Elevator<D, F> {

    enum DirectionOfTravel {
        UP, DOWN
    }

    public static DirectionOfTravel whichDirectionToTravel(int pickUp, int dropOff){
        return (pickUp > dropOff) ? DirectionOfTravel.DOWN : DirectionOfTravel.UP;
    }

    F getCurrentFloor();
    List<Integer> getFloorsVisited();
    int getNumberOfFloorVisited();
    void moveUp();
    void moveDown();
    void changeDirectionOfTravel();
    D getDirection();
    void visitTheFloor(F pickUp);
    void establishElevatorDirection(F pickUp, F dropOff);
    void registerTotalTraverse();
    void registerTraverse(F floor);
    void manipulateTraverse(F floor);
}
