package com.boa.assignment.api;

/**
 * Created by rayan on 23/06/16.
 */
public interface ElevatorControlSystem<M, D, F> {
    /**
     * Embedded enumeration DirectionOfTravel
     */
    enum ElevatorMode {
        MODE_A, MODE_B;
    }
    int getNumberOfFloorsVisited();
    void pickUp(int pickUpFloor);
    void dropOff(int dropOffFloor);
    public void visitFloors(InputCommands<ElevatorRequest<D,F>,F>... inputCommands);
    M getMode();
}
