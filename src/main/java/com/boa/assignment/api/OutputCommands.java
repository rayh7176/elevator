package com.boa.assignment.api;

/**
 * Created by rayan on 23/06/16.
 */
public interface OutputCommands<T>{
    /**
     * Accumulate the results from the calculation done by the ElevatorControlSystem
     * @param rs
     */
    void accumulateResults(T...rs);

    /**
     * Group and return the results as an array
     * @return
     */
    T [] [] getAllResults();

}
