package com.boa.assignment.api;

/**
 * Created by rayan on 23/06/16.
 */
public interface ElevatorRequest<D,F> extends Comparable<ElevatorRequest<D,F>>{
    D getDirection();
    F getPickUp();
    F getDropOff();
}
