package com.boa.assignment.api;

import java.util.function.Consumer;

/**
 * Created by rayan on 23/06/16.
 */
public interface InputCommands<T, F> {

    boolean addCommand(String pickUpDropOff);

    T poll();

    T peek();

    F initFloor();

    boolean isEmpty();
}
