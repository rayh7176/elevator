package com.boa.assignment.command;

import com.boa.assignment.api.Elevator;
import com.boa.assignment.api.ElevatorRequest;
import com.boa.assignment.model.Building;
import com.sun.media.sound.InvalidDataException;

import java.io.InvalidClassException;

/**
 * Created by rayan on 23/06/16.
 */
public class ElevatorRequestImpl implements ElevatorRequest<Elevator.DirectionOfTravel, Building.Floor> {

    final Elevator.DirectionOfTravel directionOfTravel;
    final Building.Floor pickUp;
    final Building.Floor dropOff;

    public ElevatorRequestImpl(int pickUp, int dropOff){
        this.directionOfTravel = Elevator.whichDirectionToTravel(pickUp, dropOff);
        this.pickUp = Building.Floor.valueOf(pickUp);
        this.dropOff = Building.Floor.valueOf(dropOff);
    }

    @Override
    public Elevator.DirectionOfTravel getDirection() {
        return directionOfTravel;
    }

    @Override
    public Building.Floor getPickUp() {
        return pickUp;
    }

    @Override
    public Building.Floor getDropOff() {
        return dropOff;
    }


    @Override
    public int compareTo(ElevatorRequest o) {
        if (! ( o instanceof ElevatorRequest))
            throw new RuntimeException("InvalidType Not supported "+o);
        ElevatorRequest<Elevator.DirectionOfTravel, Building.Floor> tmpRequest = (ElevatorRequest<Elevator.DirectionOfTravel, Building.Floor>)o;
        // only process the change in position if know - if directions are the same
        if (tmpRequest.getDirection().compareTo(directionOfTravel) == 0) {
            if (tmpRequest.getPickUp().compareTo(getPickUp()) >= 0){
                return tmpRequest.getDropOff().compareTo(this.getDropOff());
            }
        }
        return 1;

    }
}
