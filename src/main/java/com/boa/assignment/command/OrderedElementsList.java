package com.boa.assignment.command;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.ListIterator;

/**
 * Created by rayan on 23/06/16.
 */
public class OrderedElementsList <T extends Comparable<T>> extends LinkedList<T> {
    private static final Logger LOGGER = LoggerFactory.getLogger(OrderedElementsList.class);
    private static final long serialVersionUID = 1L;


    public boolean orderedAdd(T element) {
        ListIterator<T> itr = listIterator();
        while(true) {
            if (itr.hasNext() == false) {
                itr.add(element);
                return(true);
            }

            T elementInList = itr.next();
            if (elementInList.compareTo(element) > 0) {
                itr.previous();
                itr.add(element);
                LOGGER.info("Adding Element={}" + element);
                return(true);
            }
        }
    }
}