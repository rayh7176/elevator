package com.boa.assignment.command;

import com.boa.assignment.api.Elevator;
import com.boa.assignment.api.ElevatorRequest;
import com.boa.assignment.api.InputCommands;
import com.boa.assignment.model.Building;
import jdk.nashorn.internal.ir.annotations.Immutable;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * Created by rayan on 23/06/16.
 * This class is not Thread - safe so only sequential processing is recommended
 */
public class InputCommandsImpl implements InputCommands<ElevatorRequest<Elevator.DirectionOfTravel, Building.Floor>, Building.Floor> {

    private static final Logger LOGGER = LoggerFactory.getLogger(InputCommandsImpl.class);

    private Building.Floor initFloor;

    private OrderedElementsList<ElevatorRequest<Elevator.DirectionOfTravel, Building.Floor>> commands = new OrderedElementsList<>();

    public InputCommandsImpl(String location){
        Objects.requireNonNull(location);
        this.initFloor = Building.Floor.valueOf(location);
    }

    @Override
    public boolean addCommand(String pickDrop){
        try {
            Objects.requireNonNull(pickDrop);

            String [] input = pickDrop.split("-");

            if (input.length != 2)
                throw new Exception("Illegal Number of tokens:{"+ input.length +"}");

            String pick = input[0];
            String drop = input[1];

            Objects.requireNonNull(pick);
            Objects.requireNonNull(drop);
            if( NumberUtils.isNumber(pick)&& NumberUtils.isNumber(drop)) {
                int pickUp = Integer.parseInt(pick);
                int dropOff = Integer.parseInt(drop);
                @Immutable
                ElevatorRequest command = new ElevatorRequestImpl(pickUp,dropOff);
                this.commands.add(command);
                return true;
            }else
                throw new Exception("Pick up and Drop Off are not Numbers");

        } catch(Exception e){
            // No need to stop the system - we assume acceptable to avoid certain destinations
            LOGGER.error("Commands Input initLocation:{}, pickUp:{}, dropOff:{} are incorrect, please check error:{}",initFloor, pickDrop, e);
        }
        return false;
    }

    @Override
    public ElevatorRequest poll(){
       return this.commands.poll();
    }

    @Override
    public ElevatorRequest peek() {
        return this.commands.peek();
    }

    public boolean isEmpty(){
        return commands.size() == 0;
    }

    @Override
    public Building.Floor initFloor() {
        return initFloor;
    }


}
