package com.boa.assignment.command;

import com.boa.assignment.api.OutputCommands;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by rayan on 23/06/16.
 */
public class OutputCommandsImpl implements OutputCommands<Integer> {

    private static final Logger LOGGER = LoggerFactory.getLogger(OutputCommandsImpl.class);

    private final List<Command<Integer>> commands;

    public OutputCommandsImpl(){
        commands = new ArrayList<>();
    }

    public void accumulateResults(Integer ...rs){
        Command command = new Command();
        command.addResults(rs);
        commands.add(command);
    }

    public Integer [] [] getAllResults() {
        Integer [] [] results = new Integer[commands.size()][];
        int i =0;
        for (Command command : commands){
            Object [] data = command.getResults().toArray();
            results [i++] = Arrays.copyOf(data, data.length, Integer[].class);
        }
        return results;
    }

    private class Command<T> {

        private final List<T> results;

        private Command(){
            results = new LinkedList<>();
        }

        private void addResults(T ...ts){
            for(T t:ts){
                results.add(t);
            }
        }

        private List<T> getResults(){
            return results;
        }

        @Override
        public String toString() {
            return "Command{" +
                    "results=" + results +
                    '}';
        }
    }

}
